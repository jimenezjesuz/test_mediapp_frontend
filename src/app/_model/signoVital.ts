import {Paciente} from "./paciente";

export class SignoVital {
  'idSignoVital': number;
  'fecha': string;
  'temperatura': string;
  'ritmoRespiratorio': string;
  'pulso': string;
  'idPaciente': Paciente;
}
