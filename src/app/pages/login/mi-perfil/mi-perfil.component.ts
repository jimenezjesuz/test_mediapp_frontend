import {Component, OnInit} from '@angular/core';
import {JwtHelperService} from "@auth0/angular-jwt";
import {environment} from "../../../../environments/environment";
import {PacienteDialogComponent} from "../../signo-vital/signo-vital-edicion/paciente-dialog/paciente-dialog.component";
import {MatDialog, MatDialogRef} from "@angular/material/dialog";
import {Menu} from "../../../_model/menu";
import {MenuService} from "../../../_service/menu.service";

@Component({
  selector: 'app-mi-perfil',
  templateUrl: './mi-perfil.component.html',
  styleUrls: ['./mi-perfil.component.css']
})
export class MiPerfilComponent implements OnInit {
  userName: string;
  roles: string[];
  opciones: Menu[];

  constructor(private dialog: MatDialog, private menuService: MenuService) {
  }

  ngOnInit(): void {
    const token = sessionStorage.getItem(environment.TOKEN_NAME);
    const helper = new JwtHelperService();
    const decodedToken = helper.decodeToken(token);
    this.userName = decodedToken.user_name;
    this.roles = decodedToken.authorities;
    this.menuService.listarPorUsuario(this.userName).subscribe(data => {
      this.opciones = data;
    });
  }
}
