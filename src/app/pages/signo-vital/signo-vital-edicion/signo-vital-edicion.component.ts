import {Component, OnInit} from '@angular/core';
import {FormControl, FormGroup} from "@angular/forms";
import {Paciente} from "../../../_model/paciente";
import {Observable, Subject} from "rxjs";
import {map, switchMap} from "rxjs/operators";
import {PacienteService} from "../../../_service/paciente.service";
import {MatSnackBar} from "@angular/material/snack-bar";
import {SignoVitalService} from "../../../_service/signo-vital.service";
import {SignoVital} from "../../../_model/signoVital";
import * as moment from "moment";
import {ActivatedRoute, Params, Router} from "@angular/router";
import {Medico} from "../../../_model/medico";
import {MedicoDialogoComponent} from "../../medico/medico-dialogo/medico-dialogo.component";
import {PacienteDialogComponent} from "./paciente-dialog/paciente-dialog.component";
import {MatDialog} from "@angular/material/dialog";

@Component({
  selector: 'app-signo-vital-edicion',
  templateUrl: './signo-vital-edicion.component.html',
  styleUrls: ['./signo-vital-edicion.component.css']
})
export class SignoVitalEdicionComponent implements OnInit {
  //autocomplete
  myControlPaciente: FormControl = new FormControl();
  pacienteSeleccionado: Paciente;
  pacientesFiltrados: Observable<Paciente[]>;
  pacientes: Paciente[] = [];
  form: FormGroup;
  signoVital: SignoVital;
  id: number;
  edicion: boolean;

  pacienteCambio = new Subject<Paciente>();

  fechaSeleccionada: Date = new Date();
  maxFecha: Date = new Date();

  constructor(private pacienteService: PacienteService,
              private snackBar: MatSnackBar,
              private signoVitalService: SignoVitalService,
              private route: ActivatedRoute,
              private router: Router,
              private dialog: MatDialog) {
    this.form = new FormGroup({
      'id': new FormControl(0),
      'idPaciente': this.myControlPaciente,
      'fecha': new FormControl(new Date()),
      'temperatura': new FormControl(''),
      'ritmoRespiratorio': new FormControl(''),
      'pulso': new FormControl('')
    });
  }

  ngOnInit(): void {

    this.listarPacientes();
    this.pacientesFiltrados = this.myControlPaciente.valueChanges.pipe(
      map(val => this.filtrarPacientes(val))
    );
    this.route.params.subscribe((data: Params) => {
      this.id = data['id'];
      this.edicion = data['id'] != null;
      this.initForm();
    });
    this.pacienteService.pacienteCambioUno.subscribe(data => {
      this.pacienteSeleccionado = data;
      this.form.controls['idPaciente'].setValue(data);
      this.listarPacientes();
      this.pacientesFiltrados = this.myControlPaciente.valueChanges.pipe(map(val => this.filtrarPacientes(val)));

    });


  }

  initForm() {
    //EDITAR, por lo tanto carga la data a editar
    if (this.edicion) {
      this.signoVitalService.listarPorId(this.id).subscribe(data => {
        console.log(data);
        this.form.setValue({
          'id': data.idSignoVital,
          'idPaciente': data.idPaciente,
          'fecha': data.fecha,
          'temperatura': data.temperatura,
          'ritmoRespiratorio': data.ritmoRespiratorio,
          'pulso': data.pulso
        });
      });
    }
  }

  mostrarPaciente(val: Paciente) {
    return val ? `${val.nombres} ${val.apellidos}` : val;
  }

  seleccionarPaciente(e: any) {
    this.pacienteSeleccionado = e.option.value;
    console.log(this.pacienteSeleccionado);
  }

  filtrarPacientes(val: any) {
    if (val != null) {
      if (val.idPaciente > 0) {
        return this.pacientes.filter(el =>
          el.nombres.toLowerCase().includes(val.nombres.toLowerCase()) || el.apellidos.toLowerCase().includes(val.apellidos.toLowerCase()) || el.dni.includes(val.dni)
        );
      } else {
        return this.pacientes.filter(el =>
          el.nombres.toLowerCase().includes(val?.toLowerCase()) || el.apellidos.toLowerCase().includes(val?.toLowerCase()) || el.dni.includes(val)
        );
      }
    }
  }

  listarPacientes() {
    this.pacienteService.listar().subscribe(data => {
      this.pacientes = data;
    });
  }

  operar() {
    this.signoVital = new SignoVital();
    this.signoVital.idSignoVital = this.form.value['id'];
    this.signoVital.idPaciente = this.form.value['idPaciente'];
    this.signoVital.fecha = moment(this.form.value['fecha']).format('YYYY-MM-DDTHH:mm:ss');
    this.signoVital.temperatura = this.form.value['temperatura'];
    this.signoVital.ritmoRespiratorio = this.form.value['ritmoRespiratorio'];
    this.signoVital.pulso = this.form.value['pulso'];
    if (this.edicion) {
      this.signoVitalService.modificar(this.signoVital).pipe(switchMap(() => {
        return this.signoVitalService.listar();
      })).subscribe(data => {
        this.signoVitalService.signoVitalCambio.next(data);
        this.signoVitalService.mensajeCambio.next('Se Modificó');
      });
    } else {
      this.signoVitalService.registrar(this.signoVital).pipe(switchMap(() => {
        return this.signoVitalService.listar();
      })).subscribe(data => {
        this.signoVitalService.signoVitalCambio.next(data);
        this.signoVitalService.mensajeCambio.next('Se Registró');
      });
    }
    this.router.navigate(['signoVital']);
  }

  abrirDialogo() {
    this.dialog.open(PacienteDialogComponent, {
      width: '60%'
    });
  }

}
