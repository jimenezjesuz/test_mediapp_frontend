import {Component, OnInit, Output, EventEmitter} from '@angular/core';
import {FormBuilder, FormControl, FormGroup, Validators} from "@angular/forms";
import {Paciente} from "../../../../_model/paciente";
import {PacienteService} from "../../../../_service/paciente.service";
import {MatDialogRef} from "@angular/material/dialog";
import {switchMap} from "rxjs/operators";

@Component({
  selector: 'app-paciente-dialog',
  templateUrl: './paciente-dialog.component.html',
  styleUrls: ['./paciente-dialog.component.css']
})
export class PacienteDialogComponent implements OnInit {
  form: FormGroup;
  pacienteNew: Paciente;

  constructor(private pacienteService: PacienteService,
              public dialogRef: MatDialogRef<PacienteDialogComponent>, private fb: FormBuilder) {
    this.form = this.fb.group({
      id: [0],
      nombres: ['', [Validators.required, Validators.minLength(3)]],
      apellidos: ['', [Validators.required, Validators.minLength(3)]],
      dni: ['', [Validators.required, Validators.minLength(8), Validators.maxLength(8)]],
      telefono: ['', [Validators.required, Validators.minLength(9), Validators.maxLength(9)]],
      direccion: ['', [Validators.required, Validators.minLength(3)]]
    });
  }

  ngOnInit(): void {
  }

  get f() {
    return this.form.controls;
  }

  operar() {

    if (this.form.invalid) {
      return;
    }
    let paciente = new Paciente();
    paciente.idPaciente = this.form.value['id'];
    paciente.nombres = this.form.value['nombres'];
    paciente.apellidos = this.form.value['apellidos'];
    paciente.dni = this.form.value['dni'];
    paciente.telefono = this.form.value['telefono'];
    paciente.direccion = this.form.value['direccion'];

    this.pacienteService.registrar(paciente).subscribe(data => {
      this.pacienteService.pacienteCambioUno.next(data);
    });
    this.onCerrar();
  }

  onCerrar() {
    this.dialogRef.close();
  }
}
