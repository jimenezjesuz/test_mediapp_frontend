import {Component, OnInit, ViewChild} from '@angular/core';
import {MatSnackBar} from "@angular/material/snack-bar";
import {ActivatedRoute} from "@angular/router";
import {SignoVitalService} from "../../_service/signo-vital.service";
import {MatTableDataSource} from "@angular/material/table";
import {Especialidad} from "../../_model/especialidad";
import {MatPaginator} from "@angular/material/paginator";
import {MatSort} from "@angular/material/sort";
import {SignoVital} from "../../_model/signoVital";
import {switchMap} from "rxjs/operators";

@Component({
  selector: 'app-signo-vital',
  templateUrl: './signo-vital.component.html',
  styleUrls: ['./signo-vital.component.css']
})
export class SignoVitalComponent implements OnInit {

  cantidad: number = 0;
  displayedColumns = ['id', 'fecha', 'idPaciente', 'temperatura', 'ritmoRespiratorio', 'pulso', 'acciones'];
  dataSource: MatTableDataSource<SignoVital>;
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  constructor(private signoVitalService: SignoVitalService,
              private snackBar: MatSnackBar,
              public route: ActivatedRoute) {
  }

  filtrar(valor: string) {
    this.dataSource.filter = valor.trim().toLowerCase();
  }

  ngOnInit(): void {

    //Solo cuando se hace next()
    this.signoVitalService.signoVitalCambio.subscribe(data => {
      this.dataSource = new MatTableDataSource(data);
      this.dataSource.sort = this.sort;
      this.dataSource.paginator = this.paginator;
    });

    this.signoVitalService.mensajeCambio.subscribe(data => {
      this.snackBar.open(data, 'AVISO', {
        duration: 2000
      });
    });

    this.signoVitalService.listarPageable(0, 10).subscribe(data => {
      this.cantidad = data.totalElements;
      this.dataSource = new MatTableDataSource(data.content);
      this.dataSource.sort = this.sort;
      this.dataSource.paginator = this.paginator;
    });
  }

  eliminar(signoVital: SignoVital) {
    this.signoVitalService.eliminar(signoVital.idSignoVital).pipe(switchMap(() => {
      return this.signoVitalService.listar();
    })).subscribe(data => {
      this.signoVitalService.signoVitalCambio.next();
      this.signoVitalService.mensajeCambio.next('Se Eliminó');
    });
  }
  mostrarMas(e: any) {
    this.signoVitalService.listarPageable(e.pageIndex, e.pageSize).subscribe(data => {
      this.cantidad = data.totalElements;
      this.dataSource = new MatTableDataSource(data.content);
      this.dataSource.sort = this.sort;
    });
  }
}
